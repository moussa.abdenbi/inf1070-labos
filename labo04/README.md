# Laboratoire 4 : Commandes et fichiers

## Exercice 1 : Liens symboliques (30 mins)

### 1.1 : Création

Créez un fichier `bonjour` qui contient « `Bonjour le monde` ».

Faites un lien symbolique dans le même répertoire qui pointe vers `bonjour` et qui s'appelle `blabla`.

Afficher le contenu du répertoire avec `ls -l`.

Quelle est la taille des fichiers `bonjour` et `blabla` ?

Afficher le contenu des fichiers `bonjour` et `blabla` avec `cat`.

Éditez le fichier `blabla` pour avoir « `Salut le monde` ».

Qu'affiche maintenant `ls -l` ? Quelles tailles ont changé ? Quelles dates ont changé ?

### 1.2 : Renommage

Renommez le fichier `blabla` en `ave`.

Qu'affiche maintenant `ls -l` ?

Que se passe-t-il si on affiche le contenu de `ave` ou si on essaye de l'éditer ?

Renommez le fichier `bonjour` en `salut`.

Qu'affiche maintenant `ls -l` ?

Que se passe-t-il si on affiche le contenu de `ave` ou si on essaye de l'éditer ?

Avec l'option `-f` de la commande `ln`, faites pointer `ave` vers `salut`.

Vérifiez que `cat ave` affiche la bonne chose.

### 1.3 : Répertoires

Créez un répertoire `dir` et déplacez `ave` dedans.

Vérifiez que `dir/ave` est bien un lien symbolique vers `salut` avec la commande `stat dir/ave`.

Pourquoi `cat dir/ave` affiche « `cat: dir/ave: Aucun fichier ou dossier de ce type` »  ?

Réparez le lien symbolique `dir/ave` pour qu'il pointe correctement sur le fichier `salut`.

### 1.4 : Liens de liens de répertoires de liens de répertoires de rien...

Déplacez le fichier `salut` dans `dir` et créez un lien symbolique `salut` qui pointe sur `point_de/salut`.

Qu'affiche `tail dir/*` ? Pourquoi ?

Créez le lien symbolique `point_de` qui pointe sur `dir`. Qu'affiche maintenant `tail dir/*` ? Pourquoi ?

Faites maintenant pointer le lien symbolique `point_de` sur `dir/ave`. Qu'affiche maintenant `tail dir/*` ? Pourquoi ?

Faites maintenant pointer le lien symbolique `point_de` sur `.`. Qu'affiche maintenant `tail dir/*` ? Pourquoi ?

## Exercice 2 : Le jeu sérieux GameShell, partie 2 (90 mins)

L'objectif de cet exercice est de compléter une autre série d'exercices sur les
commandes et les fichiers.

### 2.1 : Télécharger l'archive du jeu

Dans un premier temps, récupérez l'archive qui contient le jeu. Elle se trouve
dans le répertoire courant de cet énoncé sous le nom `gameshell-labo04.tar`.

### 2.2 : Décompresser l'archive du jeu

Décompressez le fichier `gameshell-labo04.tar` en ligne de commande en
utilisant le programme `tar`.

### 2.3 : Lancer le jeu

Pour lancer le jeu, il suffit de taper la commande
```sh
./start.sh
```

Rappelons qu'en tout temps, vous pouvez quitter le jeu en entrant la commande :
```sh
exit
```
ce qui aura pour effet de sauvegarder la mission à laquelle vous êtes rendus.

Lorsque vous reprendrez le jeu, on vous demandera si vous voulez poursuivre là
où vous étiez rendus ou recommencer au début :
```
Ce n'est pas la premiere fois que vous jouez. Voulez-vous
poursuivre votre ancienne partie ? [O/n]
```
Si vous répondez `n`, votre ancienne partie sera écrasée et vous recommencerez
au début.

### 2.4 : Commandes de base

En tout temps, vous pouvez entrer les commandes suivantes pour vous aider :

- `gash help` : affiche une petite liste des commandes
- `gash HELP` : affiche une liste plus complète des commandes
- `gash show` : affiche l'objectif de la mission courante
- `gash check` : vérifie si la mission actuelle est validée
- `gash restart` : recommence la mission courante
