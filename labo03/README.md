# Laboratoire 3 : Introduction au shell

## Exercice 1 : Le jeu sérieux GameShell

L'objectif de cet exercice est de compléter un certain nombre de missions dans
le cadre d'un jeu sérieux appelé *GameShell*, développé par
[Pierre Hyvernat](http://www.lama.univ-smb.fr/~hyvernat), de l'Université de
Savoie Mont Blanc pour initier les étudiants à l'interface système (*shell* en
ang.).

## 1.1 : Télécharger l'archive du jeu

Dans un premier temps, récupérez l'archive qui contient le jeu. Elle se trouve
dans le répertoire courant de cet énoncé sous le nom `gameshell-labo03.tar`.

## 1.2 : Décompresser l'archive du jeu

Décompressez le fichier `gameshell-labo03.tar` en ligne de commande en
utilisant le programme `tar`. Consultez d'abord l'aide avec `man tar` !

## 1.3 : Lancer le jeu

Pour lancer le jeu, il suffit de taper la commande
```sh
./start.sh
```

Le texte suivant devrait alors apparaître, vous invitant à commencer le jeu.
```
============================ Initialisation de GameShell ============================
************************************************
*                                              *
*     Commencez par taper la commande          *
*       $ gash show                            *
*     pour découvrir le premier objectif       *
*     ou                                       *
*       $ gash help                            *
*     pour afficher la liste des commandes     *
*                                              *
************************************************
[mission 01] $
```

En tout temps, vous pouvez quitter le jeu en entrant la commande :
```sh
exit
```
ce qui aura pour effet de sauvegarder la mission à laquelle vous êtes rendus.

Lorsque vous reprendrez le jeu, on vous demandera si vous voulez poursuivre là
où vous étiez rendus ou recommencer au début :
```
Ce n'est pas la premiere fois que vous jouez. Voulez-vous
poursuivre votre ancienne partie ? [O/n]
```
Si vous répondez `n`, votre ancienne partie sera écrasée et vous recommencerez
au début.

## 1.4 : Commandes de base

En tout temps, vous pouvez entrer les commandes suivantes pour vous aider :

- `gash help` : affiche une petite liste des commandes
- `gash HELP` : affiche une liste plus complète des commandes
- `gash show` : affiche l'objectif de la mission courante
- `gash check` : vérifie si la mission actuelle est validée
- `gash restart` : recommence la mission courante

## Exercice 2 : Le jeu sérieux Bandit

Complétez les 10 premiers exercices du jeu
[Bandit](http://overthewire.org/wargames/bandit/).
